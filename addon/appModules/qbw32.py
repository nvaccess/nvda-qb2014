import time
import re
import os
from logHandler import log
import ui
from configobj import ConfigObj
import wx
import shlobj
import ctypes
from ctypes.wintypes import RECT
import colors
import gui
import api
import eventHandler
import scriptHandler
import winUser
import displayModel
import textInfos
import appModuleHandler
import controlTypes
from NVDAObjects import NVDAObjectTextInfo
from NVDAObjects.IAccessible import IAccessible, getNVDAObjectFromEvent
from NVDAObjects.behaviors import Dialog
from NVDAObjects.window import Window

MB_GETCHECKED=0XF0

re_qbIniSection=re.compile(r"^\s*\[(.+)\]\s*$")
re_qbIniKeyValue=re.compile(r"^\s*(?P<controlID>\d+)\s*=\s*(?P<name>[^,]*)(,\s*(?P<type>[^,]*)(,\s*(?P<keyboardShortcut>[^,]*))*)*")

def ensureQbAccessibilityMode():
	"""
	Checks if QB's accessibility mode is enabled in its ini file, and if not, returns an object that when destroied causes a modified ini file to be written.
	"""
	iniPath=os.path.join(shlobj.SHGetFolderPath(0, shlobj.CSIDL_COMMON_APPDATA),"intuit","quickBooks 2014","qbw.ini")
	lines=open(iniPath,'rt').readlines()
	modified=False #Did we modify the file? needs rewrite
	foundSection=None #Line index of the accessibility section if found or added
	foundValue=None #Linde index of the qbAccessibility value if found or added
	for index,line in enumerate(lines):
		line=line.strip()
		if line.startswith('[') and line.endswith(']'):
			if foundSection is not None:
				break
			sectionName=line[1:-1].lower()
			if sectionName=='accessibility':
				foundSection=index
				log.debug("found accessibility section at line %d"%(index+1))
		else:
			m=re.match(u'^\s*qbaccessibility\s*=\s*(.*)\s*$',line,re.IGNORECASE)
			if m:
				log.debug("Found qbAccessibility value at line %d with value %s"%(index+1,m.group(1)))
				if m.group(1)!='1':
					lines[index]='qbAccessibility=1'
					log.debug("Rewrote qbAccessibility value")
					modified=True
				foundValue=index
				break
	if foundValue is None:
		if foundSection is None:
			lines.append('[accessibility]\n')
			log.debug("Appended accessibility section")
			foundSection=len(lines)-1
		lines.append('qbAccessibility=1\n')
		log.debug("Inserted qbAccessibility value in accessibility section at line %d"%(len(lines)-1))
		modified=True
	if modified:
		class Writer(object):
			def __del__(self):
				log.debug("Writing modified file")
				try:
					with open(iniPath,'wt') as f:
						f.writelines(lines)
				except IOError:
					log.error("Could not write to quickbooks ini file to enable accessibility mode",exc_info=True)
		return Writer()

class WindowSubRectangleItem(Window):
	"""An NVDAObject that represents a rectanglular fragment of a Window (such as a focus rectangle)"""

	next=previous=firstChild=lastChild=None
	children=[]
	role=controlTypes.ROLE_PANE

	def _isEqual(self,other):
		return super(WindowSubRectangleItem,self)._isEqual(other) and self.location==other.location

	def __init__(self,parent=None,rect=None):
		"""
		@param parent: The Window object itself.
		@type parent: NVDAObject
		@param rect: a tuple of coorinates (left,top,right,bottom) describing the rectangle the object should be limted to.
		@type rect: tuple
		"""
		self.location=rect[0],rect[1],rect[2]-rect[0],rect[3]-rect[1]
		super(WindowSubRectangleItem,self).__init__(windowHandle=parent.windowHandle)
		self.parent=parent

	def _get_highlightedText(self):
		"""Retreaves any highlighted text within the object's rectangle on the window."""
		try:
			return getHighlightItemText(self)
		except LookupError:
			return None

	def _get_states(self):
		states=set()
		if self.highlightedText:
			states.add(controlTypes.STATE_SELECTED)
		return states

	def _get_name(self):
		return self.highlightedText or self.displayText

	def event_displayModel_drawFocusRectNotify(self,rect=None):
		eventHandler.executeEvent("displayModel_drawFocusRectNotify",self.parent,rect=rect)

	def script_focusMove(self,gesture):
		oldName=self.name
		oldStates=self.states
		gesture.send()
		curTime=time.time()
		timeout=curTime+0.5
		while curTime<timeout and not scriptHandler.isScriptWaiting() and not eventHandler.isPendingEvents("gainFocus"):
			api.processPendingEvents(processEventQueue=False)
			obj=self.parent.subFocusRectItemObject
			if obj and (obj!=self or obj.name!=oldName or obj.states!=oldStates):
				eventHandler.queueEvent("gainFocus",obj)
				return
			time.sleep(0.01)

	def script_activate(self,gesture):
		l=self.location
		x=(l[0]+l[2]/2)
		y=l[1]+(l[3]/2) 
		oldX,oldY=winUser.getCursorPos()
		winUser.setCursorPos(x,y)
		winUser.mouse_event(winUser.MOUSEEVENTF_LEFTDOWN,0,0,None,None)
		winUser.mouse_event(winUser.MOUSEEVENTF_LEFTUP,0,0,None,None)
		winUser.mouse_event(winUser.MOUSEEVENTF_LEFTDOWN,0,0,None,None)
		winUser.mouse_event(winUser.MOUSEEVENTF_LEFTUP,0,0,None,None)
		winUser.setCursorPos(oldX,oldY)

	__gestures={
		"kb:upArrow":"focusMove",
		"kb:downArrow":"focusMove",
		"kb:enter":"activate",
	}

class MauiForm(IAccessible):
	"""
	An NVDAObject representing a form in Quickbooks.
	Tracks focus rectangles for controls drawn directly to the window, and corrects focus for controls that share the same child window.
	"""

	role=controlTypes.ROLE_FORM
	isPresentableFocusAncestor=True

	def _get_subFocusRectItemObject(self):
		"""Fetches an NVDAObject that represents the area of this window described by its focus rectangle if it has one."""
		focusRect=displayModel.getFocusRect(self)
		if focusRect:
			return WindowSubRectangleItem(parent=self,rect=focusRect)

	def event_gainFocus(self):
		if eventHandler.isPendingEvents("gainFocus"):
			return
		obj=self.subFocusRectItemObject
		if obj:
			eventHandler.queueEvent("focusEntered",self)
			eventHandler.queueEvent("gainFocus",obj)
		else:
			super(MauiForm,self).event_gainFocus()

	def event_displayModel_drawFocusRectNotify(self,rect=None):
		if eventHandler.isPendingEvents("gainFocus"):
			return
		oldFocus=api.getFocusObject()
		if self is oldFocus or (self is oldFocus.parent and isinstance(oldFocus,WindowSubRectangleItem)):
			obj=self.subFocusRectItemObject
			if obj:
				eventHandler.queueEvent("gainFocus",obj)
			elif self is not oldFocus:
				eventHandler.queueEvent("gainFocus",self)

	def script_tab(self,gesture):
		oldFocus=api.getFocusObject()
		gesture.send()
		if not isinstance(oldFocus,IAccessible):
			return
		api.processPendingEvents(processEventQueue=False)
		if eventHandler.isPendingEvents("gainFocus") or not winUser.isWindow(oldFocus.windowHandle):
			return
		newFocus=getNVDAObjectFromEvent(oldFocus.event_windowHandle,oldFocus.event_objectID,oldFocus.event_childID)
		if newFocus.shouldAllowIAccessibleFocusEvent and newFocus!=oldFocus:
			eventHandler.executeEvent("gainFocus",newFocus)
	script_tab.canPropagate=True

	__gestures={
		"kb:tab":"tab",
		"kb:shift+tab":"tab",
	}

class QuickBooksSubForm(MauiForm):

	def _get_isPresentableFocusAncestor(self):
		return bool(self.name)

class MauiCheckBox(IAccessible):

	role=controlTypes.ROLE_CHECKBOX

	def _get_states(self):
		states=super(MauiCheckBox,self).states
		checked=ctypes.windll.user32.SendMessageW(self.windowHandle,MB_GETCHECKED,0,0)
		if checked:
			states.add(controlTypes.STATE_CHECKED)
		return states

	def script_toggleState(self,gesture):
		gesture.send()
		self.event_stateChange()

	__gestures={
		"kb:space":"toggleState",
	}

class MauiRadioButton(MauiCheckBox):
	role=controlTypes.ROLE_RADIOBUTTON

class MauuiMessageCaptionTextInfo(displayModel.DisplayModelTextInfo):
	includeDescendantWindows=False
	stripOuterWhitespace=False

class MauuiMessage(Dialog):
	role=controlTypes.ROLE_DIALOG

	def event_foreground(self):
		#The caption seems to be written to the display after the foreground event fires.
		time.sleep(0.1)
		super(MauuiMessage,self).event_foreground()

	def _get_description(self):
		return MauuiMessageCaptionTextInfo(self,textInfos.POSITION_ALL).text.strip()

class MauiPushButton(IAccessible):
	role=controlTypes.ROLE_BUTTON
	value=None

class HighlightItemTextInfo(displayModel.DisplayModelTextInfo):
	backgroundSelectionColor=colors.RGB(255,255,255)
	includeDescendantWindows=False

def getHighlightItemText(obj,leftLimit=None,topLimit=None,rightLimit=None,bottomLimit=None):
	objLocation=obj.location
	objLeft=objLocation[0]
	objTop=objLocation[1]
	objRight=objLocation[0]+objLocation[2]
	objBottom=objLocation[1]+objLocation[3]
	left=max(objLeft,leftLimit) if leftLimit is not None else objLeft
	top=max(objTop,topLimit) if topLimit is not None else objTop
	right=min(objRight,rightLimit) if rightLimit is not None else objRight
	bottom=min(objBottom,bottomLimit) if bottomLimit is not None else objBottom
	info=HighlightItemTextInfo(obj,textInfos.POSITION_SELECTION,limitRect=textInfos.Rect(left,top,right,bottom))
	return info.text

class MauiPushButtonHighlightItem(MauiPushButton):

	isPresentableFocusAncestor=True

	def event_gainFocus(self):
		listObj=self.parent.parent
		childWindow=winUser.getTopWindow(listObj.windowHandle)
		while childWindow and (not winUser.isWindowVisible(childWindow) or not winUser.isWindowEnabled(childWindow) or winUser.getClassName(childWindow)!='SysHeader32'):
			childWindow=winUser.getWindow(childWindow,winUser.GW_HWNDNEXT)
		if not childWindow:
			return super(MauiPushButtonHighlightItem,self).event_gainFocus()
		listItemObj=listObj.subFocusRectItemObject
		if not listItemObj:
			return super(MauiPushButtonHighlightItem,self).event_gainFocus()
		listItemObj.container=self
		eventHandler.queueEvent("focusEntered",self)
		eventHandler.queueEvent("gainFocus",listItemObj)

class AppModule(appModuleHandler.AppModule):

	_checkedAccessibilityMode=False

	def event_foreground(self,obj,nextHandler):
		nextHandler()
		if not self._checkedAccessibilityMode:
			self._checkedAccessibilityMode=True
			# We only support QuickBooks 2014
			v=self.productVersion
			if not v or '.' not in v or v.split('.')[0]!='24':
				log.debugWarning("This version of QuickBooks not supported: %s"%v)
				return
			try:
				writer=ensureQbAccessibilityMode()
			except IOError as e:
				# Translators: a message in Quickbooks when an error occurs setting accessibility mode
				wx.CallAfter(gui.messageBox,_("An error occured when detecting the Quickbooks Accessibility mode.\n{exception}").format(exception=e),
					# Translators:  The title of a dialog shown in Quickbooks for an error
					_("Error"))
			if writer:
				# Translators: A message to tell the user that Quickbooks must be terminated to enable accessibility mode 
				wx.CallAfter(gui.messageBox,_("In order to turn on accessibility mode in Quickbooks, NVDA has closed the application and made the needed configuration changes. Please reopen Quickbooks to continue using the application."), 
					# Translators: the title of a dialog shown when enabling accessibility mode in Quickbooks
					_("NVDA"))
				self._accessibilityModeWriter=writer
				p=ctypes.windll.kernel32.OpenProcess(1,False,obj.processID)
				if p:
					ctypes.windll.kernel32.TerminateProcess(p,0)
					ctypes.windll.kernel32.CloseHandle(p)

	def _fetchIniControlIDMap(self,path):
		controlIDMap={}
		curSection=None
		for l in iniFileContent.splitlines():
			m=re_qbIniSection.match(l)
			if m:
				sectionName=m.group(1)
				curSection=controlIDMap[sectionName]={}
				continue
			m=re_qbIniKeyValue.match(l)
			if m:
				gd=m.groupdict()
				curSection[int(gd['controlID'])]=gd
		return controlIDMap

	def __init__(self,*args,**kwargs):
		self._iniControlIDMap=self._fetchIniControlIDMap(os.path.join(os.path.dirname(__file__),"qbw32.ini"))
		super(AppModule,self).__init__(*args,**kwargs)

	def _annotateObjWithIniControlIDMapInfo(self,obj):
		controlID=obj.windowControlID
		if not obj.parent:
			return
		# Locate the real window (i.e. has a systemmenu / title bar)
		mainWindow=Window._get_parent(obj)
		while mainWindow and mainWindow.appModule is self:
			if mainWindow.windowStyle&winUser.WS_SYSMENU:
				break
			mainWindow=Window._get_parent(mainWindow)
		else:
			return
		sectionName=mainWindow.name
		if not sectionName:
			return
		sectionName=sectionName.split('-')[0]
		sectionName=sectionName.split(':')[0]
		sectionName=sectionName.replace(' ','')
		windowControlIDMap=self._iniControlIDMap.get(sectionName)
		if not windowControlIDMap:
			return
		info=windowControlIDMap.get(controlID)
		if not info:
			return
		name=info.get('name')
		if name:
			obj.name=name.strip()
		keyboardShortcut=info.get('keyboardShortcut')
		if keyboardShortcut:
			obj.keyboardShortcut=keyboardShortcut.strip()

	def event_NVDAObject_init(self,obj):
		if isinstance(obj,IAccessible) and obj.event_objectID==-4 and obj.windowControlID is not None:
			self._annotateObjWithIniControlIDMapInfo(obj)

	def chooseNVDAObjectOverlayClasses(self,obj,clsList):
		if not isinstance(obj,IAccessible) or obj.event_objectID!=winUser.OBJID_CLIENT:
			return
		windowClassName=obj.windowClassName
		if windowClassName=="MauiPushButton":
			clsList.insert(0,MauiPushButton)
			if obj.windowControlID==100:
				clsList.insert(0,MauiPushButtonHighlightItem)
		elif windowClassName=="MauuiMessage":
			clsList.insert(0,MauuiMessage)
		elif windowClassName=="MauiCheckBox":
			clsList.insert(0,MauiCheckBox)
		elif windowClassName=="MauiRadioButton":
			clsList.insert(0,MauiRadioButton)
		elif windowClassName in ("MauiForm","MauiPopUp"):
			clsList.insert(0,MauiForm)
		elif windowClassName=="QuickBooksSubForm":
			clsList.insert(0,QuickBooksSubForm)

iniFileContent="""
[CreateInvoices]
603=Customer:Job, Edit Combo, Alt + J
605=Account, Edit Combo
607=Class, Edit Combo
696=Template, Edit Combo
610=Date, Date Edit
612=Invoice #, Edit
615=Bill To, Multi-Line Edit
622=Ship To, Multi-Line Edit
678=Due Date, Date Edit
1=Item, Edit Combo
10=Quantity, Edit
13=Price Each, Edit
14=Amount, Edit
26=Tax, Edit Combo
2=Description, Edit
4=Tax, Edit Combo
643=Memo, Edit
722=Online Pay, Edit Combo
660=Customer Tax Code, Edit Combo, Alt + X
628=P.O. No., Edit
630=Terms, Edit Combo
682=Contract #, Edit
632=Rep, Edit Combo
634=Ship, Edit
636=Via, Edit Combo
638=F.O.B., Edit
8=Class, Edit Combo
703=S.O. No., Edit
706=Ship To, Edit Combo
101=Customer Message, Edit Combo, Alt + M
104=Tax, Edit Combo
[EnterSalesReceipts]
603=Customer:Job, Edit Combo, Alt + J
607=Class, Edit Combo
696=Template, Edit Combo
610=Date, Date Edit
612=Sale No., Edit
615=Sold To, Multi Line Edit
628=Check No., Edit
630=Payment Method
1=Item
2=Description
10=Quantity
13=Rate
14=Amount
26=Tax
4=Tax
643=Memo, Edit
660=Customer Tax Code, Edit Combo, Alt + X
101=Customer Message, Edit Combo, Alt + M
104=Tax, Edit Combo
[CreateEstimates]
425=Class, Edit Combo
460=Template, Edit Combo
408=Date, Date Edit
410=Estimate #, Edit
415=Name / Address, Multi-line Edit
471=Ship To, Multi-line Edit
0=Item, Edit Combo
1=Description, Edit
9=QTY, Edit
11=Cost, Edit
13=Markup, Edit
14=Total, Edit
15=Tax, Edit Combo
3=Tax, Edit Combo
405=Memo, Edit
427=Customer Tax Code, Edit Combo, Alt + X
101=Customer Message, Edit Combo, Alt + M
103=Tax, Edit Combo
403=Customer:Job, Edit Combo, Alt + J
476=Ship To, Edit Combo
[CreateSalesOrders]
30003=Customer:Job, Edit Combo, Alt + J
30025=Class, Edit Combo
30008=Date, Edit
30010=S.O. No., Edit
30015=Name / Address, Multi-Line Edit
30081=Ship To, Edit Combo
30070=Ship To, Multi-line Edit
30043=P.O. No, Edit
0=Item, Edit Combo
9=Ordered, Edit
11=Rate, Edit
7=Class, Edit Combo
12=Amount, Edit
13=Tax, Edit Combo
1=Description, Edit
3=Tax, Edit Combo
30005=Memo, Edit
30027=Customer Tax Code, Edit Combo, Alt + X
30060=Template, Edit Combo
30051=Ship Date, Date Edit
30053=Ship Via, Edit Combo
30055=F O B, Edit
30049=Rep, Edit Combo
101=Customer Message, Edit Combo, Alt + M
103=Tax, Edit Combo
[BatchInvoice]
26=Tax, Edit Combo
1=Item Code, Edit Combo
2=Description, Edit
30351=Look for, Edit
31531=Billing Group, Combo box
610=Date, Edit
696=Template, Combo Box
10=Quantity, Edit
13=Price Each, Edit
8=Class, Edit Combo
14=Amount, Edit
101=Customer Message, Edit Combo
[CreateStatements]
7004=To, Date Edit
7019=Template, Edit Combo
7016=Statement Date, Date Edit, Alt + S
7003=Statement Period From, Date Edit
7054=Include only transactions over x days past due date, Edit
7059=with a balance less than, Edit
[AssessFinanceCharges]
11803=Assessment Date, Date Edit
[ReceivePayments]
5603=Received From, Edit Combo
5612=Payment Method, Edit Combo
5613=Reference #, Edit
9=Payment, Edit
5604=Amount, Edit
5673=Card No., Edit
5676=Exp. Date, Edit
5681=Year, Edit
5605=Date, Edit
5615=Memo, Edit
[CreateCreditMemos/Refunds]
610=Date, Date Edit
612=Credit No., Edit
615=Customer, Multi line Edit
603=Customer:Job, Edit Combo, Alt + J
607=Class, Edit Combo
696=Template, Edit Combo
628=P.O. Number, Edit
1=Item, Edit Combo
2=Description, Edit
10=QTY, Edit
13=Rate, Edit Combo
14=Amount, Edit
26=Tax, Edit Combo
101=Customer Message, Edit Combo, Alt + M
104=Tax, Edit Combo
643=Memo, Edit
660=Customer Tax Code, Edit Combo, Alt + X
[WeeklyTimesheet]
1=Service Item, Edit Combo
5=Notes, Edit
0=Customer:Job, Edit Combo
6=Class
7=Monday, Edit
65281=Billable
321=Name, Edit Combo
2=Payroll Item, Edit Combo
3=W C Code, Edit Combo
8=Tuesday, Edit
9=Wednesday, Edit
10=Thursday, Edit
11=Friday, Edit
12=Saturday, Edit
13=Sunday, Edit
65285=Billable?, Check Box
310=Calendar, Button
[Time/EnterSingleActivity]
102=Name, Edit Combo
103=Customer:Job, Edit Combo
104=Service Item, Edit Combo
106=Payroll Item, Edit Combo
108=W C Code, Edit Combo
110=Notes, Multi Line Edit
101=Date, Date Edit
109=Duration, Edit
[ItemList]
30351=Look for, Edit, Alt + K
[ChangeItemPrices]
5=New Price, Edit
65281=based on
[Find]
3632=Customer:Job, Edit Combo
3633=Date, Date Edit
3634=To, Date Edit
3636=Invoice #, Edit
3638=Amount, Edit, Alt + M
3553=Account,Edit Combo
3601=Include split detail?, Edit Combo
3581=Number, Edit
3582=To, Edit
3541=Job Type, Edit Combo
3591=Name Street1
3532=From,Date Edit,Alt+M
3533=To,Date Edit
3596=Name Zip, Edit
[EnterBills]
304=Account, Edit Combo
309=Vendor, Edit Combo
308=Date, Date Edit
307=Ref. No., Edit
310=Amount Due, Edit
322=Bill Due, Date Edit
346=Terms, Edit Combo
586=Class, Edit Combo
317=Memo, Edit
0=Account, Edit Combo
1=Amount, Edit
3=Memo, Edit
4=Customer:Job, Edit Combo
6=Cost, Edit
312=Address, Multi-line Edit
2=QTY, Edit
[PayBills]
65283=Check box
8=Amt. To Pay, Edit
3182=Filter by, Edit Combo
3102=Account, Edit Combo
3104=Date, Date Edit
[PaySalesTax]
9902=Check Date, Date Edit
9903=Show sales tax due through, Date Edit
9911=Starting Check No., Edit
[SalesTaxCheck-Checking]
304=Bank Account, Edit Combo
307=No., Edit
308=Date, Date Edit
309=Pay to the order of, Edit Combo
310=Amount, Edit
312=Address, Multi line Edit
[SalesTaxRevenueSummary]
65284=Sort By
2=From, Date Edit
3=To, Date Edit
[SalesTaxLiability]
2=From, Date Edit
3=To, Date Edit
[CreatePurchaseOrders]
603=Vendor, Edit Combo
607=Class, Edit Combo
666=Drop Ship to, Multi line Edit
696=Template, Edit Combo
610=Date, Date Edit
612=P.O. NO., Edit
65284=Vendor, Edit Combo
622=Ship To, Multi line Edit
1=Item, Edit
2=Description, Edit
10=Qty, Edit
13=Rate, Edit
25=Customer, Edit Combo
14=Amount, Edit
615=Vendor, Edit Combo
643=Memo, Edit
706=Ship To, Edit Combo
102=Vendor Message, Multi Line Edit, Alt + M
[CreateItemReceipts]
309=Vendor, Edit Combo
308=Date, Date Edit
307=Ref. No, Edit
310=Total, Edit
586=Class, Edit Combo
317=Memo, Edit
1=Amount, Edit
3=Memo, Edit
4=Customer:Job, Edit Combo
5=Billable?, Check box
6=Class, Edit Combo
2=QTY, Edit
7=Amount, Edit
11=Customer:Job, Edit Combo
[InventoryCenter]
27=Show/Hide Details, Button
1414=Enter Search String, Edit
1413=Search, Button
112=Attachments, Button
101=Edit, Button
353=Name, Edit
354=Description, Edit
357=Mfg Part No, Edit
361=Cost, Edit
363=Sales Price, Edit
364=Markup, Edit
479=Margin, Edit
1828=Note:, EDit
[Notepad]
3452=Notes for Item, Multi line Edit
[CustomerCenter]
101=Edit, Button
112=Attachments, Button
306=Bill To, Edit
1828=Notes, Multi line Edit
27=Show/Hide Details, Button
1414=Enter Search String, Edit
1413=Search, Button
[EditAccount]
115=Number, Edit
136=Account Name, Edit, Alt + M
140=Description, Multi line edit, Alt + D
142=Account No., Edit
138=Subaccount of, Edit Combo, Alt + S
151=Routing Number, Edit
65282=
[EnterVehicleMileage]
24101=Vehicle, Edit Combo
24102=Trip Start Date, Date Edit
24103=Trip End Date, Date Edit
24104=Odometer Start, Edit
24105=Odometer End, Edit
24106=Total Miles, Edit
24110=Customer:Job, Edit Combo
24111=Item, Edit Combo
24112=Class, Edit Combo
24113=Notes, Edit
[NewAccount]
115=Number, Edit
136=Account Name, Edit, Alt + M
140=Description, Multi line edit, Alt + D
142=Account No., Edit
138=Subaccount of, Edit Combo, Alt + S
151=Routing Number, Edit
[VendorCenter]
1414=Enter Search Item, Edit
1413=Search, Button
27=Show/Hide Details, Button
101=Edit, Button
112=Attachments, Button
306=Billed From, Date Edit
[NewItem]
0=Item, Edit Combo
902=Item Name/Number, Edit
904=Subitem of, Edit Combo
911=Description on Sales Transactions, Multi line edit
915=Sales Price, Edit
917=Income Account
1036=Unit of Measure U/M Set, Edit Combo
916=Tax Code, Edit Combo, Alt +X
65282=Type, Edit Combo
906=Manufacturer's Part Number, Edit
905=Description on Purchase Transactions, Multi line edit
908=Cost, Edit
909=C O G S Account, Edit Combo
910=Preferred Vendor, Edit Combo
918=Inventory Information Asset Account, Edit Combo
919=Reorder Point (min), Edit
1063=Max, Edit
931=On Hand, Edit
934=Total Value, Edit
936=As Of, Date Edit
2=QTY, Edit
943=Tax Code, Edit Combo
920=Payment Method, Edit Combo
955=Deposit To, Edit Combo
914=Description, Edit
1026=Purchase Description, Edit
1004=Date, Date Edit
1013=Vendor/Payee, Edit
1028=Asset Account, Edit Combo
1027=Sales Description, Edit
1014=Sales Date, Date Edit
1015=Sales Expense, Edit Combo
1010=Asset Information Asset Description, Edit
1012=Location, Edit
1007=PO Number, Edit
1008=Serial Number, Edit
1011=Warranty Expires, Date Edit
1001=Notes, Edit
921=Tax Agency (vendor that you collect for, Edit Combo
[EditItem]
0=Item, Edit Combo
902=Item Name/Number, Edit
904=Subitem of, Edit Combo
911=Description on Sales Transactions, Multi line edit
915=Sales Price, Edit
917=Income Account, Edit Combo
1036=Unit of Measure U/M Set, Edit Combo
916=Tax Code, Edit Combo, Alt +X
65282=Type, Edit Combo
906=Manufacturer's Part Number, Edit
905=Description on Purchase Transactions, Multi line edit
908=Cost, Edit
909=C O G S Account, Edit Combo
910=Preferred Vendor, Edit Combo
918=Inventory Information Asset Account, Edit Combo
919=Reorder Point (min), Edit
1063=Max, Edit
931=On Hand, Edit
934=Total Value, Edit
936=As Of, Date Edit
2=QTY, Edit
943=Tax Code, Edit Combo
920=Payment Method, Edit Combo
955=Deposit To, Edit Combo
914=Description, Edit
1026=Purchase Description, Edit
1004=Date, Date Edit
1013=Vendor/Payee, Edit
1028=Asset Account, Edit Combo
1027=Sales Description, Edit
1014=Sales Date, Date Edit
1015=Sales Expense, Edit Combo
1010=Asset Information Asset Description, Edit
1012=Location, Edit
1007=PO Number, Edit
1008=Serial Number, Edit
1011=Warranty Expires, Date Edit
1001=Notes, Edit
[SetupCustomFieldsforItems]
14903=Label
[NewUser]
16126=User Name, Edit, Alt + U
16127=Password (optional), Edit, Alt + P
16128=Confirm Password, Edit, Alt + C
16133=Description, Read only Edit
[QuickBooksPro2014]
116428032=Select a company that you've previously opened and click Open
112084176=
[NewVendor]
1001=Vendor Name, Edit
1022=Opening Balance, Edit
1040=As of, Date Edit
1055=Company Name, Edit
1056=Salutation, Edit
1035=First Name, Edit
1036=Middle Initial, Edit
1037=Last Name, Edit
613=Job Title, Edit
1004=Address Details Billed From, Multi-line Edit
1012=Shipped From, Multi line edit
1030=Account No., Edit
1018=Credit Limit, Edit
1020=Payment Terms, Edit Combo
1378=Billing Rate Level, Edit Combo
1027=Print Name On Check As, Edit
1026=Vendor Tax ID, Edit
1453=Expense Account 1, Edit Combo
1454=Expense Account 2, Edit Combo
1455=Expense Account 3, Edit Combo
1017=Vendor Type, Edit Combo
14682=Custom Fields Discount Available, Edit
638=Field 1, Edit
640=Field 2, Edit
642=Field 3, Edit
644=Field 4, Edit
646=Field 5, Edit
648=Field 6, Edit
650=Field 7, Edit
652=Field 8, Edit
[EditVendor]
1001=Vendor Name, Edit
1022=Opening Balance, Edit
1040=As of, Date Edit
1055=Company Name, Edit
1056=Salutation, Edit
1035=First Name, Edit
1036=Middle Initial, Edit
1037=Last Name, Edit
613=Job Title, Edit
1004=Address Details Billed From, Multi-line Edit
1012=Shipped From, Multi line edit
1030=Account No., Edit
1018=Credit Limit, Edit
1020=Payment Terms, Edit Combo
1378=Billing Rate Level, Edit Combo
1027=Print Name On Check As, Edit
1026=Vendor Tax ID, Edit
1453=Expense Account 1, Edit Combo
1454=Expense Account 2, Edit Combo
1455=Expense Account 3, Edit Combo
1017=Vendor Type, Edit Combo
14682=Custom Fields Discount Available, Edit
638=Field 1, Edit
640=Field 2, Edit
642=Field 3, Edit
644=Field 4, Edit
646=Field 5, Edit
648=Field 6, Edit
650=Field 7, Edit
652=Field 8, Edit
[NewPriceLevel]
100=Price Level Name, Edit, Alt +N
104=item prices by, Edit Combo
[EditPriceLevel]
100=Price Level Name, Edit, Alt +N
104=item prices by, Edit Combo
[NewSalesTaxCode]
2101=Sales Tax Code (maximum 3 characters), Edit
2102=Description, Edit
[EditSalesTaxCode]
2101=Sales Tax Code (maximum 3 characters), Edit
2102=Description, Edit
[NewClass]
401=Class Name, Edit, Alt + M
403=Subclass of, Edit Combo, Alt + S
[EditClass]
401=Class Name, Edit, Alt + M
403=Subclass of, Edit Combo, Alt + S
[NewWorkersCompensationCode]
110=Code, Edit, Alt + D
120=Description, Edit, Alt + i
160=Rate, Edit, Alt + T
170=Start using rate on, Date Edit, Alt + S
[EditWorkersCompensationCode]
110=Code, Edit, Alt + D
120=Description, Edit, Alt + i
160=Rate, Edit, Alt + T
170=Start using rate on, Date Edit, Alt + S
[NewName]
1001=Name, Edit
1055=Company Name, Edit
1056=Mr./Ms./.., Edit
1035=First Name, Edit
1036=M. I., Edit
1037=Last Name, Edit
1004=Name and Address, Multi line edit
1002=Contact, Edit
1009=Phone, Edit
1011=Fax, Edit
1010=Alt. Ph. Edit
1003=Alt. Contact, Edit
1083=E-mail, Edit
1030=Account, No., Edit
[EditName]
1001=Name, Edit
1055=Company Name, Edit
1056=Mr./Ms./.., Edit
1035=First Name, Edit
1036=M. I., Edit
1037=Last Name, Edit
1004=Name and Address, Multi line edit
1002=Contact, Edit
1009=Phone, Edit
1011=Fax, Edit
1010=Alt. Ph. Edit
1003=Alt. Contact, Edit
1083=E-mail, Edit
1030=Account, No., Edit
[ScheduleMemorizedTransaction]
4801=Name, Edit, Alt + N
4804=Next Date, Edit, Alt + X
4811=Number Remaining, Edit, Alt + I
4813=Days In Advance to Enter, Edit, Alt + S
[NewVehicle]
3901=Vehicle, Edit, Alt + I
3902=Description, Multi line Edit, Alt + D
[EditVehicle]
3901=Vehicle, Edit, Alt + I
3902=Description, Multi line Edit, Alt + D
[NewShippingMethod]
1681=Shipping Method, Edit, Alt + M[NewPaymentMethod]
10301=Payment Method, Edit, Alt + M
51=Payment Type, Edit Combo, Alt + T
[EditShippingMethod]
1681=Shipping Method, Edit, Alt + M[NewPaymentMethod]
10301=Payment Method, Edit, Alt + M
51=Payment Type, Edit Combo, Alt + T
[NewCustomerMessage]
1691=Message, Multi line Edit, Alt + M
[EditCustomerMessage]
1691=Message, Multi line Edit, Alt + M
[NewTerms]
3801=Terms, Edit, Alt + T
3805=Net Due in X Days.,Edit
3808=Discount Percentage is, Edit
3811=Discount if paid within x Day., Edit
3814=Net Due Before the xth day of the month., Edit
3824=Due the next month if issued with X days of due date., Edit
3817=Discount percentage is, Edit
3820=Discount if paid before the xth day of the month., edit
[EditTerms]
3801=Terms, Edit, Alt + T
3805=Net Due in X Days.,Edit
3808=Discount Percentage is, Edit
3811=Discount if paid within x Day., Edit
3814=Net Due Before the xth day of the month., Edit
3824=Due the next month if issued with X days of due date., Edit
3817=Discount percentage is, Edit
3820=Discount if paid before the xth day of the month., edit
[NewJobType]
176=Job Type Name, Edit, Alt + T
178=Subtype of, Edit Combo, Alt + S
[EditJobType]
176=Job Type Name, Edit, Alt + T
178=Subtype of, Edit Combo, Alt + S
[NewVendorType]
1701=Vendor Type, Edit, Alt + T
1703=Subtype of, Edit Combo, Alt + S
[EditVendorType]
1701=Vendor Type, Edit, Alt + T
1703=Subtype of, Edit Combo, Alt + S
[NewCustomerType]
1701=Customer Type, Edit, Alt + T
1703=Subtype of, Edit, Alt + S
[EditCustomerType]
1701=Customer Type, Edit, Alt + T
1703=Subtype of, Edit, Alt + S
[WriteChecks]
304=Bank Account, Edit Combo, Alt + K
307=No., Edit
308=Date, Date Edit
309=Pay To The Order Of, Edit Combo
310=$, Edit
312=Address, Multi line Edit
317=Memo, Edit
[AddNewAccount]
136=Account Name, Edit, Alt + M
138=Subaccount of, Edit Combo, Alt + S
140=Description, Multi line Edit, Alt + D
142=Note, Edit
[MakeDeposits]
0=Received From, Edit Combo
1=From Account, Edit Combo
2=Memo, Edit
3=Check Number, Edit
4=PMT Meth., Edit Combo
5=Class, Edit Combo
6=Amount, Edit Combo
2310=Cash back goes to, Edit Combo
2312=Cash Back Memo, Edit
2311=Cash Back Amount, Edit
2302=Deposit To Checking, Edit Combo, Alt + D
2303=Date, Date Edit
2305=Memo, Edit
[TransferFundsBetweenAccounts]
22608=Date, Date Edit
22601=Transfer Funds From, Edit Combo, Alt + M
22604=Transfer Funds To, Edit Combo, Alt + T
607=Class, Edit Combo
22606=Transfer Amount, Edit
22607=Memo, Edit
[EnterCreditCardCharges]
304=Credit Card, Edit Combo
309=Purchased From, Edit Combo, Alt + D
308=Date, Date Edit
307=Ref No, Edit
310=Amount, Edit
317=Memo, Edit
[BeginReconciliation]
4242=Account, Edit Combo, Alt + A
4243=Statement Date, Date Edit, Alt + D
4247=Ending Balance, Edit, Alt + B
4251=Finance Charge, Edit, Alt + F
4252=Date, Date Edit, Alt + T
4257=Account, Edit Combo, Alt + O
4259=Class, Edit Combo, Alt + L
4260=Interest Earned, Edit, Alt + I
4261=Date, Date Edit, Alt + E
4266=Account, Edit Combo, Alt + U
4268=Class, Edit Combo
[EnterCardInformation]
14405=Payment, Edit Combo
14382=Card Number, Edit
14384=Exp Month, Edit
14387=Exp Year, Edit
[NewPaymentMethod]
10301=Payment Method, Edit, Alt + M
[QuickBooksLogin]
15924=Password: Edit
[AdjustQuantity/ValueonHand]
12801=Adjustment Date, Date Edit, Alt + D
12802=Adjustment Account, Edit Combo, Alt + M
12804=Reference No., Edit
12812=Customer:Job, Edit Combo, Alt + J
12806=Class, Edit Combo
1=Item, Edit Combo
8=New Quantity, Edit
9=QTY Difference, Edit
12803=Memo, Edit
[EditAddressInformation]
2932=Address, Multi line Edit
2937=City, Edit
2938=State / Province, Edit
2939=Zip / Postal Code, Edit
2940=Country / Region, Edit
2941=Note, Edit
[SalesTaxAdjustment]
9951=Adjustment Date, Date Edit, Alt + D
9963=Class, Edit Combo, Alt + S
9953=Entry No., Edit, Alt + N
9954=Sales Tax Vendor, Edit Combo, Alt + V
9955=Adjustment Account, Edit Combo, Alt + A
9956=Amount, Edit, Alt + O
9961=Memo, Edit, Alt + M
[SelectItemReceipt]
12751=Vendor, Edit Combo, Alt + V
[NewCustomer]
1001=Customer Name, Edit
1022=Opening Balance, Edit
1040=As Of, Date Edit
1055=Company Name, Edit
1056=Salutation, Edit
1035=First Name,E dit
1036=Middle Initial, Edit
1037=Last Name, Edit
613=Job Title, Edit
638=Custom Field 1, Edit
640=Custom Field 2, Edit
642=Custom Field 3, Edit
644=Custom Field 4, Edit
646=Custom Field 5, Edit
648=Custom Field 6, Edit
650=Custom Field 7, Edit
652=Custom Field 8, Edit
1004=Invoice/Bill To, Multi line Edit
1084=Edit Invoice/Bill To Address, Button
1092=Ship To, Edit Combo
65283=Add Ship To Address, Button
1085=Edit Ship To Address, Button
1093=Delete Ship To Address, Button
1012=Ship To, Multi line Edit
1030=Account No. Edit
1018=Credit Limit, Edit
1020=Payment Terms, Edit Combo
1086=Price Level, Edit Combo
1466=Help, Button
1240=Preferred Payment Method, Edit Combo
1467=Help, Button
1242=Credit Card No., Edit
1244=Expiration Month, Edit
1252=Expiration Year, Edit
1246=Name On Card, Edit
1248=Address, Edit
1250=Zip / Postal Code, Edit
1021=Tax Code, Edit Combo, Alt + X
636=Help, Button
1032=Tax Item, Edit Combo
1039=Resale No., Edit
1017=Customer Type, Edit Combo
1031=Rep, Edit Combo
14681=Contract #, Edit
14683=B-Day, Edit
14685=Spouses Name, Edit
1051=Job Description, Edit
1059=Job Type, Edit Combo
1028=Start Date, Date Edit
1047=Projected End Date, Date Edit
1048=End Date, Date Edit
[AddShippingAddressInformation]
2946=Address Name, Edit
2932=Address, Multi line Edit
2937=City, Edit
2938=State / Province, Edit
2939=Zip / Postal Code, Edit
2940=Country / Region, Edit
2941=Note, Edit
[EditCustomer]
1001=Customer Name, Edit
1022=Opening Balance, Edit
1040=As Of, Date Edit
1055=Company Name, Edit
1056=Salutation, Edit
1035=First Name,E dit
1036=Middle Initial, Edit
1037=Last Name, Edit
613=Job Title, Edit
638=Custom Field 1, Edit
640=Custom Field 2, Edit
642=Custom Field 3, Edit
644=Custom Field 4, Edit
646=Custom Field 5, Edit
648=Custom Field 6, Edit
650=Custom Field 7, Edit
652=Custom Field 8, Edit
1004=Invoice/Bill To, Multi line Edit
1084=Edit Invoice/Bill To Address, Button
1092=Ship To, Edit Combo
65283=Add Ship To Address, Button
1085=Edit Ship To Address, Button
1093=Delete Ship To Address, Button
1012=Ship To, Multi line Edit
1030=Account No. Edit
1018=Credit Limit, Edit
1020=Payment Terms, Edit Combo
1086=Price Level, Edit Combo
1466=Help, Button
1240=Preferred Payment Method, Edit Combo
1467=Help, Button
1242=Credit Card No., Edit
1244=Expiration Month, Edit
1252=Expiration Year, Edit
1246=Name On Card, Edit
1248=Address, Edit
1250=Zip / Postal Code, Edit
1021=Tax Code, Edit Combo, Alt + X
636=Help, Button
1032=Tax Item, Edit Combo
1039=Resale No., Edit
1017=Customer Type, Edit Combo
1031=Rep, Edit Combo
14681=Contract #, Edit
14683=B-Day, Edit
14685=Spouses Name, Edit
1051=Job Description, Edit
1059=Job Type, Edit Combo
1028=Start Date, Date Edit
1047=Projected End Date, Date Edit
1048=End Date, Date Edit
[AddShippingAddressInformation]
2946=Address Name, Edit
2932=Address, Multi line Edit
2937=City, Edit
2938=State / Province, Edit
2939=Zip / Postal Code, Edit
2940=Country / Region, Edit
2941=Note, Edit
[EditJob]
1001=Job Name, Edit
1022=Opening Balance, Edit
1040=As Of, Date Edit
1055=Company Name, Edit
1056=Salutation, Edit
1035=First Name,E dit
1036=Middle Initial, Edit
1037=Last Name, Edit
613=Job Title, Edit
638=Custom Field 1, Edit
640=Custom Field 2, Edit
642=Custom Field 3, Edit
644=Custom Field 4, Edit
646=Custom Field 5, Edit
648=Custom Field 6, Edit
650=Custom Field 7, Edit
652=Custom Field 8, Edit
1004=Invoice/Bill To, Multi line Edit
1084=Edit Invoice/Bill To Address, Button
1092=Ship To, Edit Combo
65283=Add Ship To Address, Button
1085=Edit Ship To Address, Button
1093=Delete Ship To Address, Button
1012=Ship To, Multi line Edit
1030=Account No. Edit
1018=Credit Limit, Edit
1020=Payment Terms, Edit Combo
1086=Price Level, Edit Combo
1466=Help, Button
1240=Preferred Payment Method, Edit Combo
1467=Help, Button
1242=Credit Card No., Edit
1244=Expiration Month, Edit
1252=Expiration Year, Edit
1246=Name On Card, Edit
1248=Address, Edit
1250=Zip / Postal Code, Edit
1021=Tax Code, Edit Combo, Alt + X
636=Help, Button
1032=Tax Item, Edit Combo
1039=Resale No., Edit
1017=Customer Type, Edit Combo
1031=Rep, Edit Combo
14681=Contract #, Edit
14683=B-Day, Edit
14685=Spouses Name, Edit
1051=Job Description, Edit
1059=Job Type, Edit Combo
1028=Start Date, Date Edit
1047=Projected End Date, Date Edit
1048=End Date, Date Edit
1023=Customer, Edit Combo
[NewJob]
1001=Job Name, Edit
1022=Opening Balance, Edit
1040=As Of, Date Edit
1055=Company Name, Edit
1056=Salutation, Edit
1035=First Name,E dit
1036=Middle Initial, Edit
1037=Last Name, Edit
613=Job Title, Edit
638=Custom Field 1, Edit
640=Custom Field 2, Edit
642=Custom Field 3, Edit
644=Custom Field 4, Edit
646=Custom Field 5, Edit
648=Custom Field 6, Edit
650=Custom Field 7, Edit
652=Custom Field 8, Edit
1004=Invoice/Bill To, Multi line Edit
1084=Edit Invoice/Bill To Address, Button
1092=Ship To, Edit Combo
65283=Add Ship To Address, Button
1085=Edit Ship To Address, Button
1093=Delete Ship To Address, Button
1012=Ship To, Multi line Edit
1030=Account No. Edit
1018=Credit Limit, Edit
1020=Payment Terms, Edit Combo
1086=Price Level, Edit Combo
1466=Help, Button
1240=Preferred Payment Method, Edit Combo
1467=Help, Button
1242=Credit Card No., Edit
1244=Expiration Month, Edit
1252=Expiration Year, Edit
1246=Name On Card, Edit
1248=Address, Edit
1250=Zip / Postal Code, Edit
1021=Tax Code, Edit Combo, Alt + X
636=Help, Button
1032=Tax Item, Edit Combo
1039=Resale No., Edit
1017=Customer Type, Edit Combo
1031=Rep, Edit Combo
14681=Contract #, Edit
14683=B-Day, Edit
14685=Spouses Name, Edit
1051=Job Description, Edit
1059=Job Type, Edit Combo
1028=Start Date, Date Edit
1047=Projected End Date, Date Edit
1048=End Date, Date Edit
1023=Customer, Edit Combo
[AddShippingAddressInformation]
2946=Address Name, Edit
2932=Address, Multi line Edit
2937=City, Edit
2938=State / Province, Edit
2939=Zip / Postal Code, Edit
2940=Country / Region, Edit
2941=Note, Edit
[Add/EditMultipleListEntries]
13=Find, Edit
101=Name, Edit
107=Company Name, Edit
108=Mr./Ms./Mrs..., Edit
109=First Name, Edit
110=Middle Initial, Edit
111=Last Name, Edit
112=Main Phone, Edit
114=FAX, EDit
113=Alt. Phone, Edit
122=E-mail, Edit
166=CC Email, Edit
117=Bill To 1, Edit
118=Bill To 2, Edit
119=Bill To 3, Edit
120=Bill To 4, Edit
121=Bill To 5, EDit
12=Search, Button
18=Delete Search String, Button
"""
